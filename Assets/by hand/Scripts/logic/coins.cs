﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coins : MonoBehaviour {

    public Text coinLabel;
    public void Update()
    {
        coinLabel.text = PlayerPrefs.GetInt("SavedCoins").ToString();
    }
}
