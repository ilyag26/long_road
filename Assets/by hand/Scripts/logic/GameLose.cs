﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class GameLose : MonoBehaviour
{
    public GameObject layout,
                      explosion1, explosion2, explosion3, explosion4,
                      car1, car2, car3, car4;
    public float timer = 3;
    public float timer2 = 5;
    public float timer3 = 5;
    public static float life = 30;
    public Text lifeLabel;
    public Text timerLabel;
    [SerializeField]
    private GameObject LifeLine30;
    [SerializeField]
    private GameObject LifeLine25;
    [SerializeField]
    private GameObject LifeLine20;
    [SerializeField]
    private GameObject LifeLine15;
    [SerializeField]
    private GameObject LifeLine10;
    [SerializeField]
    private GameObject LifeLine5;
    [SerializeField]
    private GameObject TextFuel;

    // check lose
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            if (timer >= 0)
            {
              
                timer -= Time.deltaTime;
             
            }
            if (timer <= 0)
            {

                timer2 -= Time.deltaTime;
                timerLabel.text = Math.Round(timer2).ToString();

            }
            if (timer2 <= 0)
            {
                timerLabel.text = " ";
                timer2 = 0;
                life -= Time.deltaTime;
                lifeLabel.text = Math.Round(life).ToString();
            }

        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            timer = 3;
            timer2 = 5;
            timerLabel.text = " ";
        }
    }
 
    public void Update()
    {

        if (life <= 30)
        {
            LifeLine30.SetActive(true);
            LifeLine25.SetActive(true);
            LifeLine20.SetActive(true);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (life <= 25)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(true);
            LifeLine20.SetActive(true);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (life <= 20)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(true);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (life <= 15)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (life <= 10)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(false);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(true);
        }
        if (life <= 5)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(false);
            LifeLine10.SetActive(false);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(true);
        }
        if (life <= 0)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(false);
            LifeLine10.SetActive(false);
            LifeLine5.SetActive(false);
            TextFuel.SetActive(true);
        }
    }
    public void Start()
    {
        life = 30;
        LifeLine30.SetActive(true);
        LifeLine25.SetActive(true);
        LifeLine20.SetActive(true);
        LifeLine15.SetActive(true);
        LifeLine10.SetActive(true);
        LifeLine5.SetActive(true);
        TextFuel.SetActive(false);
    }
}