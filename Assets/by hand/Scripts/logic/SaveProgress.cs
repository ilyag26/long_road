﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveProgress : MonoBehaviour {

    int counter;

    private void OnTriggerEnter(Collider other)
    {
        if (GameLogic.check == true)
        {
            counter = PlayerPrefs.GetInt("SavedLevels");
            counter = counter + 1;
            PlayerPrefs.SetInt("SavedLevels", counter);
        }
          
    }


public void restart()
{
    
        PlayerPrefs.DeleteAll();
    
}
}
