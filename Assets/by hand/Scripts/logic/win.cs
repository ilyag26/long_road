﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class win : MonoBehaviour {

    public Text winLabel;
    public Text gainLabel;
    public void Update()
    {
        gainLabel.text = earnMoney.coins.ToString();
        winLabel.text = GameLogic.level.ToString();
    }
}
