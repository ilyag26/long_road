﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnStopButton : MonoBehaviour {

    public void ExitGame() {
        Application.Quit();
    }

    public void PlayGame(GameObject obj)
    {
        obj.GetComponent<Pause>().isPaused = false;       
    }

    public void StopGame(GameObject obj)
    {
        obj.GetComponent<Pause>().isPaused = true;
    }

    public void StopVolume(GameObject obj)
    {  
            obj.GetComponent<SoundControl>().VolumeStoped = !obj.GetComponent<SoundControl>().VolumeStoped;
    }

}
