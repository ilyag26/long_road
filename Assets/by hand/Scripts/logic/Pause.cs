﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {

    [HideInInspector]
    public bool isPaused;
    [SerializeField]
    private KeyCode pauseButton;
    [SerializeField]
    private GameObject panelPause;
    [SerializeField]
    private GameObject Player;
    [SerializeField]
    private GameObject Joistick;
    [SerializeField]
    private GameObject StopButton;
    [SerializeField]
    private GameObject ReturnButton;
    [SerializeField]
    private GameObject ExitButton;
    [SerializeField]
    private GameObject CameraChangeButton;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(pauseButton))
        {
            isPaused = !isPaused;
        }

        if (isPaused)
        {
            AudioListener.pause = true;
            panelPause.SetActive(true);
            Joistick.SetActive(false);
            StopButton.SetActive(false);
            ReturnButton.SetActive(true);
            ExitButton.SetActive(true);
            CameraChangeButton.SetActive(true);
            Time.timeScale = 0;
          
        }
        else
        {
            panelPause.SetActive(false);
            Joistick.SetActive(true);
            StopButton.SetActive(true);
            ReturnButton.SetActive(false);
            ExitButton.SetActive(false);
            CameraChangeButton.SetActive(false);
            Time.timeScale = 1;
            if (GetComponent<SoundControl>().VolumeStoped == true)
            {
                AudioListener.pause = true;
            }
            else
            {
                AudioListener.pause = false;
            }

        }
	}
}
