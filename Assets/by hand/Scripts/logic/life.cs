﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class life : MonoBehaviour {
    public GameObject petrol;
    public Text lifeLabel;
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            petrol.gameObject.SetActive(false);
            if (GameLose.life < 30)
            {
                GameLose.life = GameLose.life + 2;
                lifeLabel.text = Math.Round(GameLose.life).ToString();
            }

        }
    }
}
