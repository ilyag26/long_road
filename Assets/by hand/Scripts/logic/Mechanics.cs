﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CnControls;

public class Mechanics : MonoBehaviour
{
    float speed = 11;
    Vector3 position;
    private void Start()
    {
        
    }
    void FixedUpdate()
    {
        position = new Vector3(CnInputManager.GetAxis("Horizontal"),0f, CnInputManager.GetAxis("Vertical"));
        transform.position += position * speed* Time.deltaTime;
    }
}
