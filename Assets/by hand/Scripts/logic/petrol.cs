﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class petrol : MonoBehaviour
{
    public GameObject layout,
                      explosion1, explosion2, explosion3, explosion4,
                      car1, car2, car3, car4;
    public float timer = 3;
    public float timer2 = 5;
    public float timer3 = 5;
    public bool bul, bul2, bul3, bul4;
    public Text lifeLabel;
    public Text timerLabel;
    [SerializeField]
    private GameObject LifeLine30;
    [SerializeField]
    private GameObject LifeLine25;
    [SerializeField]
    private GameObject LifeLine20;
    [SerializeField]
    private GameObject LifeLine15;
    [SerializeField]
    private GameObject LifeLine10;
    [SerializeField]
    private GameObject LifeLine5;
    [SerializeField]
    private GameObject TextFuel;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameLose.life -= Time.deltaTime;
            lifeLabel.text = Math.Round(GameLose.life).ToString();
            if (GameLose.life <= 0)
            {
                GameLose.life = 0;
                bul = true;
                
            }
        }
    }

    public void Update()
    {
        if (bul == true)
        {     
                Application.LoadLevel(2); 
        }
        if (GameLose.life <= 30)
        {
            LifeLine30.SetActive(true);
            LifeLine25.SetActive(true);
            LifeLine20.SetActive(true);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (GameLose.life <= 25)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(true);
            LifeLine20.SetActive(true);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (GameLose.life <= 20)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(true);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (GameLose.life <= 15)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(true);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(false);
        }
        if (GameLose.life <= 10)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(false);
            LifeLine10.SetActive(true);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(true);
        }
        if (GameLose.life <= 5)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(false);
            LifeLine10.SetActive(false);
            LifeLine5.SetActive(true);
            TextFuel.SetActive(true);
        }
        if (GameLose.life <= 0)
        {
            LifeLine30.SetActive(false);
            LifeLine25.SetActive(false);
            LifeLine20.SetActive(false);
            LifeLine15.SetActive(false);
            LifeLine10.SetActive(false);
            LifeLine5.SetActive(false);
            TextFuel.SetActive(true);
        }
    }
    public void Start()
    {
        GameLose.life = 30;
        LifeLine30.SetActive(true);
        LifeLine25.SetActive(true);
        LifeLine20.SetActive(true);
        LifeLine15.SetActive(true);
        LifeLine10.SetActive(true);
        LifeLine5.SetActive(true);
        TextFuel.SetActive(false);
    }
}
