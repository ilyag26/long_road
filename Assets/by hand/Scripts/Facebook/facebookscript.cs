﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using UnityEngine.UI;
public class facebookscript : MonoBehaviour {
    public Text friendsLabel;
    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(() =>
            {

                if (FB.IsInitialized)
                    FB.ActivateApp();
                else
                    Debug.LogError("Couldn't initialize");
            },
            isGameShown =>
            {
                if (!isGameShown)
                    Time.timeScale = 0;
                else
                    Time.timeScale = 1;
            });
        }
        else
            FB.ActivateApp();
    }
    #region Login/Logout
    public void FacebookLogin()
    {
        var permissions = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(permissions);
    }
    public void FacebookLogout()
    {
        FB.LogOut();
    }
        #endregion
    public void FacebookShare()
    {
        FB.ShareLink(new System.Uri("http://ourclass217.wordpress.com"),"Download!");
    }
    #region Invite
    public void FacebookGameRequest()
    {
        FB.AppRequest("Download this cool gae,Bro!",title:"Lonely road");
    }
    public void FacebookInvite()
    {
        FB.Mobile.AppInvite(new System.Uri("http://ourclass217.wordpress.com"));
    }
    #endregion
    public void FacebookGetFriends()
    {
        string query = "/me/friends";
        FB.API(query, HttpMethod.GET, result => {
        var dictionary = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);
        var friendsList = (List<object>)dictionary["data"];
        friendsLabel.text = string.Empty;
        foreach (var dict in friendsList)
                friendsLabel.text += ((Dictionary<string, object>)dict)["name"];
        });
    }
}
