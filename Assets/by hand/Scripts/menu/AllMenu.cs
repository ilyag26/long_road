﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllMenu : MonoBehaviour {
    [SerializeField]
    private GameObject Alert;
    [SerializeField]
    private GameObject MainMenu;
    [SerializeField]
    private GameObject LevelChose;
    [SerializeField]
    private GameObject Shop;
    [SerializeField]
    private GameObject Settings;
    [SerializeField]
    public int Open;
    [SerializeField]
    private GameObject Point;
    public int Trust;
    private void Start()
    {
        Trust = PlayerPrefs.GetInt("Point");
        if (PlayerPrefs.GetInt("Point")==1){
            Alert.active = false;
            MainMenu.active = true;
            LevelChose.active = false;
            Shop.active = false;
            Settings.active = false;
        }
        else
        {

            Alert.active = true;
            MainMenu.active = false;
            LevelChose.active = false;
            Shop.active = false;
            Settings.active = false;
        }

    }

    private void Update()
    {
        if(Open == 1)
        {
            Alert.SetActive(false);
            MainMenu.SetActive(false);
            LevelChose.SetActive(true);
            Settings.SetActive(false);
            Shop.SetActive(false);
        }
        if (Open == 2)
        {
            Alert.SetActive(false);
            MainMenu.SetActive(false);
            LevelChose.SetActive(false);
            Settings.SetActive(true);
            Shop.SetActive(false);
        }
        if (Open == 3)
        {
            Alert.SetActive(false);
            MainMenu.SetActive(false);
            LevelChose.SetActive(false);
            Settings.SetActive(false);
            Shop.SetActive(true);
        }
        if (Open == 4)
        {
            Alert.SetActive(false);
            MainMenu.SetActive(true);
            LevelChose.SetActive(false);
            Settings.SetActive(false);
            Shop.SetActive(false);
        }
        if (Open == 5)
        {
            Application.Quit();
        }
        if (Point.GetComponent<Toggle>().isOn == true)
        {
            PlayerPrefs.SetInt("Point",1);
        }
    }
  
}
