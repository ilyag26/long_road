﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour {

    [HideInInspector]
    public bool VolumeStoped;
	
	// Update is called once per frame
	void Update () {
		if (VolumeStoped == true)
        {
            AudioListener.pause = true;
        }
        else {
            AudioListener.pause = false;
        }
	}
}
