﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motionCar : MonoBehaviour
{
    public static float rotation = 90.939f;
    public static float speed = 1;
    public static Vector3 position = new Vector3(0,0,1);
    void Update()
    {
        transform.Translate(position * speed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(0,rotation, 0);
    }

    public void restart()
    {
      rotation = 90.939f;
    }
}
