﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonMenu : MonoBehaviour {
    public GameObject reset;

    public void ToLevelChose(GameObject obj){
        obj.GetComponent<AllMenu>().Open = 1;
    }

    public void ToSettings(GameObject obj)
    {
        obj.GetComponent<AllMenu>().Open = 2;
    }

    public void ToShop(GameObject obj)
    {
        obj.GetComponent<AllMenu>().Open = 3;
    }

    public void ToMainMenu(GameObject obj)
    {
        obj.GetComponent<AllMenu>().Open = 4;
    }

    public void Exit(GameObject obj)
    {
        obj.GetComponent<AllMenu>().Open = 5;
    }
    public void reset1()
    {
        reset.SetActive(false);
    }
}
