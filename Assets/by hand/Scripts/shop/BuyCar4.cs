﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyCar4 : MonoBehaviour
{
    public GameObject car4;
    public GameObject car4hidden;

    void Update()
    {
        if (Input.GetMouseButton(0) && shop.reset4 == true)
        {
            if (PlayerPrefs.GetInt("SavedCoins") >= 150)
            {
                car4.SetActive(true);
                car4hidden.SetActive(false);
                PlayerPrefs.SetInt("SavedCoins", PlayerPrefs.GetInt("SavedCoins") - 150);
                PlayerPrefs.SetString("CarBought4", "Open4");
                shop.reset4 = false;
            } else {
                shop.reset4 = false;
            }
        }
    }
}
