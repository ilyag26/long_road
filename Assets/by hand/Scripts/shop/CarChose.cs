﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarChose : MonoBehaviour {
    public GameObject Car11,Car22,Car33, Car44,Car55,Car66,Camera1,Camera2,Camera3, Camera4,Camera5,Camera6;
    void Start()
    {
        if (PlayerPrefs.GetInt("CarSelect") == 1)
        {
            Car11.SetActive(true);
            Car22.SetActive(false);
            Car33.SetActive(false);
            Car44.SetActive(false);
            Car55.SetActive(false);
            Car66.SetActive(false);
            Camera1.SetActive(true);
            Camera2.SetActive(false);
            Camera3.SetActive(false);
            Camera4.SetActive(false);
            Camera5.SetActive(false);
            Camera6.SetActive(false);
        }
        if (PlayerPrefs.GetInt("CarSelect2") == 1)
        {
            Car11.SetActive(false);
            Car22.SetActive(true);
            Car33.SetActive(false);
            Car44.SetActive(false);
            Car55.SetActive(false);
            Car66.SetActive(false);
            Camera1.SetActive(false);
            Camera2.SetActive(true);
            Camera3.SetActive(false);
            Camera4.SetActive(false);
            Camera5.SetActive(false);
            Camera6.SetActive(false);
        }
        if (PlayerPrefs.GetInt("CarSelect3") == 1)
        {
            Car11.SetActive(false);
            Car22.SetActive(false);
            Car33.SetActive(true);
            Car44.SetActive(false);
            Car55.SetActive(false);
            Car66.SetActive(false);
            Camera1.SetActive(false);
            Camera2.SetActive(false);
            Camera3.SetActive(true);
            Camera4.SetActive(false);
            Camera5.SetActive(false);
            Camera6.SetActive(false);
        }
        if (PlayerPrefs.GetInt("CarSelect4") == 1)
        {
            Car11.SetActive(false);
            Car22.SetActive(false);
            Car33.SetActive(false);
            Car44.SetActive(true);
            Car55.SetActive(false);
            Car66.SetActive(false);
            Camera1.SetActive(false);
            Camera2.SetActive(false);
            Camera3.SetActive(false);
            Camera4.SetActive(true);
            Camera5.SetActive(false);
            Camera6.SetActive(false);
        }
        if (PlayerPrefs.GetInt("CarSelect5") == 1)
        {
            Car11.SetActive(false);
            Car22.SetActive(false);
            Car33.SetActive(false);
            Car44.SetActive(false);
            Car55.SetActive(true);
            Car66.SetActive(false);
            Camera1.SetActive(false);
            Camera2.SetActive(false);
            Camera3.SetActive(false);
            Camera4.SetActive(false);
            Camera5.SetActive(true);
            Camera6.SetActive(false);
        }
        if (PlayerPrefs.GetInt("CarSelect6") == 1)
        {
            Car11.SetActive(false);
            Car22.SetActive(false);
            Car33.SetActive(false);
            Car44.SetActive(false);
            Car55.SetActive(false);
            Car66.SetActive(true);
            Camera1.SetActive(false);
            Camera2.SetActive(false);
            Camera3.SetActive(false);
            Camera4.SetActive(false);
            Camera5.SetActive(false);
            Camera6.SetActive(true);
        }
    }
}
