﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectCar4 : MonoBehaviour {
    public GameObject Car1, Car2, Car3, Car4, Car5, Car6, Car1Hidden, Car2Hidden, Car3Hidden, Car4Hidden, Car5Hidden, Car6Hidden;
    void Update()
    {
        if (Input.GetMouseButton(0) && shop.select4 == true)
        {
            Car1.SetActive(false);
            Car1Hidden.SetActive(true);
            PlayerPrefs.SetInt("CarSelect", 0);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(false);
                Car2Hidden.SetActive(true);
                PlayerPrefs.SetInt("CarSelect2",0);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(false);
                Car3Hidden.SetActive(true);
                PlayerPrefs.SetInt("CarSelect3", 0);

            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(true);
                Car4Hidden.SetActive(false);
                PlayerPrefs.SetInt("CarSelect4", 1);

            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(false);
                Car5Hidden.SetActive(true);
                PlayerPrefs.SetInt("CarSelect5", 0);

            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(false);
                Car6Hidden.SetActive(true);
                PlayerPrefs.SetInt("CarSelect6", 0);

            }
            shop.select4 = false;
        }
    }
}
