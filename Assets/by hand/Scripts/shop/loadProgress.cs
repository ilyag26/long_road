﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadProgress : MonoBehaviour {

    public GameObject car2, car3, car4,car5,car6, car2hidden, car3hidden,car4hidden, car5hidden, car6hidden;
    public GameObject Car1, Car2, Car3, Car4,Car5,Car6, Car1Hidden, Car2Hidden, Car3Hidden, Car4Hidden,Car5Hidden,Car6Hidden;

    void Start () {
        if (PlayerPrefs.GetInt("CarSelect2") == 0 || PlayerPrefs.GetInt("CarSelect3") == 0
    || PlayerPrefs.GetInt("CarSelect4") == 0 || PlayerPrefs.GetInt("CarSelect5") == 0
    || PlayerPrefs.GetInt("CarSelect6") == 0)
        {
            PlayerPrefs.SetInt("CarSelect", 1);
        }
        if (PlayerPrefs.GetString("CarBought2") == "Open2"){
            car2.SetActive(true);
            car2hidden.SetActive(false); 
        }
        if (PlayerPrefs.GetString("CarBought3") == "Open3")
        {
            car3.SetActive(true);
            car3hidden.SetActive(false);
           
        }
        if (PlayerPrefs.GetString("CarBought4") == "Open4")
        {
            car4.SetActive(true);
            car4hidden.SetActive(false);

        }
        if (PlayerPrefs.GetString("CarBought5") == "Open5")
        {
            car5.SetActive(true);
            car5hidden.SetActive(false);

        }
        if (PlayerPrefs.GetString("CarBought6") == "Open6")
        {
            car6.SetActive(true);
            car6hidden.SetActive(false);

        }
        if (PlayerPrefs.GetInt("CarSelect") == 1)
        {
            Car1.SetActive(true);
            Car1Hidden.SetActive(false);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(false);
                Car2Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(false);
                Car3Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(false);
                Car4Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(false);
                Car5Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(false);
                Car6Hidden.SetActive(true);
            }
        }
        if (PlayerPrefs.GetInt("CarSelect2") == 1)
        {
            Car1.SetActive(false);
            Car1Hidden.SetActive(true);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(true);
                Car2Hidden.SetActive(false);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(false);
                Car3Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(false);
                Car4Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(false);
                Car5Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(false);
                Car6Hidden.SetActive(true);
            }
        }
        if (PlayerPrefs.GetInt("CarSelect3") == 1)
        {
            Car1.SetActive(false);
            Car1Hidden.SetActive(true);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(false);
                Car2Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(true);
                Car3Hidden.SetActive(false);
            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(false);
                Car4Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(false);
                Car5Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(false);
                Car6Hidden.SetActive(true);
            }

        }
        if (PlayerPrefs.GetInt("CarSelect4") == 1)
        {
            Car1.SetActive(false);
            Car1Hidden.SetActive(true);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(false);
                Car2Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(false);
                Car3Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(true);
                Car4Hidden.SetActive(false);
            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(false);
                Car5Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(false);
                Car6Hidden.SetActive(true);
            }
        }
        if (PlayerPrefs.GetInt("CarSelect5") == 1)
        {
            Car1.SetActive(false);
            Car1Hidden.SetActive(true);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(false);
                Car2Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(false);
                Car3Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(false);
                Car4Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(true);
                Car5Hidden.SetActive(false);
            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(false);
                Car6Hidden.SetActive(true);
            }
        }
        if (PlayerPrefs.GetInt("CarSelect6") == 1)
        {
            Car1.SetActive(false);
            Car1Hidden.SetActive(true);
            if (PlayerPrefs.GetString("CarBought2") == "Open2")
            {
                Car2.SetActive(false);
                Car2Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought3") == "Open3")
            {
                Car3.SetActive(false);
                Car3Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought4") == "Open4")
            {
                Car4.SetActive(false);
                Car4Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought5") == "Open5")
            {
                Car5.SetActive(false);
                Car5Hidden.SetActive(true);
            }
            if (PlayerPrefs.GetString("CarBought6") == "Open6")
            {
                Car6.SetActive(true);
                Car6Hidden.SetActive(false);
            }
        }
    }

}
