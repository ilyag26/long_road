﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyCar5 : MonoBehaviour
{
    public GameObject car5;
    public GameObject car5hidden;

    void Update()
    {
        if (Input.GetMouseButton(0) && shop.reset5 == true)
        {
            if (PlayerPrefs.GetInt("SavedCoins") >= 150)
            {
                car5.SetActive(true);
                car5hidden.SetActive(false);
                PlayerPrefs.SetInt("SavedCoins", PlayerPrefs.GetInt("SavedCoins") - 250);
                PlayerPrefs.SetString("CarBought5", "Open5");
                shop.reset5 = false;
            } else {
                shop.reset5 = false;
            }
        }
    }
}
