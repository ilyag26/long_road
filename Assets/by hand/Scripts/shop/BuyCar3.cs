﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyCar3 : MonoBehaviour
{
    public GameObject car3;
    public GameObject car3hidden;

    void Update()
    {
        car3hidden.SetActive(true);
        if (Input.GetMouseButton(0) && shop.reset3 == true)
        {
            if (PlayerPrefs.GetInt("SavedCoins") >= 300)
            {
                car3.SetActive(true);
                car3hidden.SetActive(false);
                PlayerPrefs.SetInt("SavedCoins", PlayerPrefs.GetInt("SavedCoins") - 300);
                PlayerPrefs.SetString("CarBought3", "Open3");
                shop.reset3 = false;
            }
            else
            {
                shop.reset3 = false;
            }
        }
    }
}
