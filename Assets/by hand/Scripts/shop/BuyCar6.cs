﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyCar6 : MonoBehaviour
{
    public GameObject car6;
    public GameObject car6hidden;

    void Update()
    {
        if (Input.GetMouseButton(0) && shop.reset6 == true)
        {
            if (PlayerPrefs.GetInt("SavedCoins") >= 150)
            {
                car6.SetActive(true);
                car6hidden.SetActive(false);
                PlayerPrefs.SetInt("SavedCoins", PlayerPrefs.GetInt("SavedCoins") - 300);
                PlayerPrefs.SetString("CarBought6", "Open6");
                shop.reset6 = false;
            } else {
                shop.reset6 = false;
            }
        }
    }
}
