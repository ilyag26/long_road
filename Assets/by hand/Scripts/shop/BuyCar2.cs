﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyCar2 : MonoBehaviour
{
    public GameObject car2;
    public GameObject car2hidden;
    void Update()
    {   
        car2hidden.SetActive(true);
        if (Input.GetMouseButton(0) && shop.reset2 == true)
        {
            if (PlayerPrefs.GetInt("SavedCoins") >= 50)
            {
                car2.SetActive(true);
                car2hidden.SetActive(false);    
                PlayerPrefs.SetInt("SavedCoins", PlayerPrefs.GetInt("SavedCoins") - 50);
                PlayerPrefs.SetString("CarBought2", "Open2");
                shop.reset2 = false;
            }
            else
            {
                shop.reset2 = false;
            }
        }
    }
}
