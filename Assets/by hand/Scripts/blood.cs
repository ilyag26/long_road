﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blood : MonoBehaviour {

    public Material ShaderTexture;

    private void OnRenderImage(RenderTexture cameraViev, RenderTexture shaderView)
    {
        Graphics.Blit(cameraViev, shaderView, ShaderTexture);
    }
}
