﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GraficSettings : MonoBehaviour {

    public Dropdown dropDown;

	// Use this for initialization
	void Start () {
        //QualitySettings.names;
        dropDown.AddOptions(QualitySettings.names.ToList());
        if (PlayerPrefs.HasKey("Quality"))
        {
            dropDown.value = PlayerPrefs.GetInt("Quality");
            QualitySettings.SetQualityLevel(PlayerPrefs.GetInt("Quality"));
        }
        else
        {
            dropDown.value = QualitySettings.GetQualityLevel();
        }
    }
	
	// Update is called once per frame
	public void SetQuality () {
        QualitySettings.SetQualityLevel(dropDown.value);
        PlayerPrefs.SetInt("Quality", dropDown.value);
	}
}
